<?php
/**
 * Created by PhpStorm.
 * User: jpeconi
 * Date: 1/19/2016
 * Time: 10:48 AM
 *
 *
 *
 * Combo A (Fried rice, chicken ball, egg roll, bbq pork) ($10.00)
 * Combo B (Chow Mien, egg roll, Chicken ball) ($9.00)
 * Spicy Sichuan Noodles ($10.50)
 * Golden Noodle ($15.00)
 * Egg roll ($1.89)
 *
 * Clicking the submit button on the page "POSTs" the form to processOrder.php
 *
 * On processOrder.php you will calculate the total price for all the items.  Use define() to declare the prices.
 *
 * Print the sub-total
 *
 * Print the tax amount (12%)
 *
 * Print the total price and give the date and time of the order.
 *
 *
 *
 */
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Golden Noodle</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link href="noodle.css" rel="stylesheet">
    <script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.8.0.js"></script>
    <script src="noodle.js" type="text/javascript"></script>
</head>
<body>
<div id="content">
    <img src="logo.gif" id="logo">
    <fieldset>
        <legend>Golden Noodle Order Form</legend>
        <form action="processOrder.php" method="post">
            <label>Combo A (Fried rice, chicken ball, egg roll, bbq pork) ($10.00)</label>
            <br>
            <input type="text" class="form-control, col-s-4" placeholder="Quantity" name="comboA" id="a">
            <br>
            <label>Combo B (Chow Mien, egg roll, Chicken ball) ($9.00)</label>
            <br>
            <input type="text" class="form-control, col-s-4" placeholder="Quantity" name="comboB">
            <br>
            <label>Spicy Sichuan Noodles ($10.50)</label>
            <br>
            <input type="text" class="form-control, col-s-4" placeholder="Quantity" name="spicy">
            <br>
            <label>Golden Noodle ($15.00)</label>
            <br>
            <input type="text" class="form-control, col-s-4" placeholder="Quantity" name="golden">
            <br>
            <label>Egg roll ($1.89)</label>
            <br>
            <input type="text" class="form-control, col-s-4" placeholder="Quantity" name="eggRoll">
            <br>
            <br>
            <button type="submit" class="btn btn-danger" id="submit">Order</button>
            <button type="button" class="btn btn-success" id="clear">clear</button>
        </form>
    </fieldset>
</div>
</body>
</html>
