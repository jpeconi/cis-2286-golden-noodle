/**
 * Created by jpeconi on 1/19/2016.
 */
$(document).ready(function () {

    var isValid = false;
    var formElements = [];


    $('#clear').click(function () {
        $('form').trigger("reset")
    });

    $('#submit').click(function () {

        $("form :input").each(function () {
            formElements.push($(this).val());
        });

        for (var x = 0; x < formElements.length - 2; x++) {
            if (formElements[x].length > 0) {
                isValid = true;
            }
        }
    })

});