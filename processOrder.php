<?php
/**
 * Created by PhpStorm.
 * User: jpeconi
 * Date: 1/19/2016
 * Time: 10:48 AM
 *
 *
 *
 * Combo A (Fried rice, chicken ball, egg roll, bbq pork) ($10.00)
 * Combo B (Chow Mien, egg roll, Chicken ball) ($9.00)
 * Spicy Sichuan Noodles ($10.50)
 * Golden Noodle ($15.00)
 * Egg roll ($1.89)
 *
 * Clicking the submit button on the page "POSTs" the form to processOrder.php
 *
 * On processOrder.php you will calculate the total price for all the items.  Use define() to declare the prices.
 *
 * Print the sub-total
 *
 * Print the tax amount (12%)
 *
 * Print the total price and give the date and time of the order.
 *
 *
 *
 */

date_default_timezone_set("America/halifax");
define("comboAPrice", 10.0);
define("comboBPrice", 9.0);
define("spicyPrice", 10.5);
define("goldenPrice", 15);
define("eggRollPrice", 1.89);
define("taxPercent", .12);

$comboA = $_POST['comboA'];
$comboB = $_POST['comboB'];
$spicy = $_POST['spicy'];
$golden = $_POST['golden'];
$eggRoll = $_POST['eggRoll'];
$length = count($_POST);
$isEmptyValid = false;
$isNumeric = true;


foreach ($_POST as $x) {
    if(!empty($x)) {
        $isEmptyValid = true;
    } else if(!is_numeric($x)) {
        $isNumeric = false;
    }
}

if(!$isEmptyValid) {
    echo "<h1>You didnt order anything!</h1>";
    exit;
} else if(!$isNumeric) {
    echo "<h1>Numbers only please!</h1>";
    exit;
}



$comboATotal = $comboA * comboAPrice;
$comboBTotal = $comboB * comboBPrice;
$spicyTotal = $spicy * spicyPrice;
$goldenTotal = $golden * goldenPrice;
$eggRollTotal = $eggRoll * eggRollPrice;

$subTotal = $comboATotal + $comboBTotal + $spicyTotal + $goldenTotal + $eggRollTotal;
$taxAmt = $subTotal * taxPercent;
$total = $subTotal + $taxAmt;

echo "<h1>Thank You</h1>";
echo "<h3>Your order has been processed</h3>";
echo "<hr>";
echo "Your subtotal is : $" . number_format((float)$subTotal, 2, '.', '');
echo "<br>";
echo "Your taxes are : $" . number_format((float)$taxAmt, 2, '.', '');
echo "<br>";
echo "For a grand total of : $" . number_format((float)$total, 2, '.', '');
echo "<br>";
echo "Order processed at " . date('g:iA - T') . " on " . date('l, F d, Y');


?>


